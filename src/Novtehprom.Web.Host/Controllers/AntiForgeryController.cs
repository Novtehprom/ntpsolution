using Microsoft.AspNetCore.Antiforgery;
using Novtehprom.Controllers;

namespace Novtehprom.Web.Host.Controllers
{
    public class AntiForgeryController : NovtehpromControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
