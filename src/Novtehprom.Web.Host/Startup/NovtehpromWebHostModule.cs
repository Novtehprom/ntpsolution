﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Novtehprom.Configuration;

namespace Novtehprom.Web.Host.Startup
{
    [DependsOn(
       typeof(NovtehpromWebCoreModule))]
    public class NovtehpromWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public NovtehpromWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(NovtehpromWebHostModule).GetAssembly());
        }
    }
}
