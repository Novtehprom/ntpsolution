﻿using Abp.Localization;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Novtehprom.Authorization.Roles;
using Novtehprom.Authorization.Users;
using Novtehprom.MultiTenancy;
using Novtehprom.WebPages.BaseTypes;
using Novtehprom.WebPages.Contacts;
using Novtehprom.WebPages.WebSites;
using System;
using System.ComponentModel.DataAnnotations;

namespace Novtehprom.EntityFrameworkCore
{
    public class NovtehpromDbContext : AbpZeroDbContext<Tenant, Role, User, NovtehpromDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public NovtehpromDbContext(DbContextOptions<NovtehpromDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Пункты первой части прайса
        /// </summary>
        public virtual DbSet<WebPriceBody> WebPriceBodies { get; set; }
        /// <summary>
        /// Период действия прайса
        /// </summary>
        public virtual DbSet<WebPricePeriod> WebPricePeriods { get; set; }
        /// <summary>
        /// Пункты второй части прайса
        /// </summary>
        public virtual DbSet<WebPriceBodyNext> WebPriceBodyNexts { get; set; }
        /// <summary>
        /// Таблица с одной записью. Информация о компании. Контакты
        /// </summary>
        public virtual DbSet<Contact> Contacts { get; set; }

        /// <summary>
        /// Для настройки контекста: сущностей, связей и т.п.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Contact>(e =>
            {
                e.OwnsOne(c => c.BossFio).Property(b => b.Name).HasColumnName("BossName").HasMaxLength(256);
                e.OwnsOne(c => c.BossFio).Property(b => b.Patronymic).HasColumnName("BossPatronymic").HasMaxLength(256);
                e.OwnsOne(c => c.BossFio).Property(b => b.Surname).HasColumnName("BossSurname").HasMaxLength(256);
            });

            var languageName = "ru";
            var languageNameEn = "en";

            modelBuilder.Entity<Contact>().HasData(
                new Contact[]
                {
                    new Contact
                    {
                        Id = 1,
                        CreatorUserId =1,
                        NameFull = "ContacrNameFull",
                        AddressReg =  "ContactAddressReg",
                        INN = "3234045475",
                        KPP = "325701001",
                        BankName = "ContactBankName",
                        Account = "40702810602380000513",
                        CorrAccount = "30101810100000000787",
                        BainkBIK = "044525787",
                        AddressFact = "ContactAddressFact",
                        OGRN = "1023202747622",
                        OKPO = "57332029",
                        OKVED = "51.53.24",
                        NameShort = "ContactNameShort",
                        AboutCompanyText = "ContactAboutCompanyText",
                    }
                }
            );

            modelBuilder.Entity<Contact>().OwnsOne(e => e.BossFio)
                .HasData(
                    new 
                    { 
                        ContactId = 1, 
                        Name = "ContactBossName", 
                        Surname = "ContactBossSurname", 
                        Patronymic = "ContactBossPatronymic" 
                    });

            modelBuilder.Entity<Communication>()
                .HasData(
                    new Communication[]
                    {   
                        new Communication { Id=1, CreatorUserId=1, ContactId =1, OrderNum = 1, Name = "ContactCommunicationName1", Value = "8(483)2-571337" },
                        new Communication { Id=2, CreatorUserId=1, ContactId =1, OrderNum = 2, Name = "ContactCommunicationName2", Value = "+7(910)330-68-20" },
                        new Communication { Id=3, CreatorUserId=1, ContactId =1, OrderNum = 3, Name = "ContactCommunicationName3", Value = "novtehprom@yandex.ru" },
                        new Communication { Id=4, CreatorUserId=1, ContactId =1, OrderNum = 4, Name = "ContactCommunicationName4", Value = "lavlav281" }
                    });

            var dtNow = DateTime.Now;

            modelBuilder.Entity<WebPricePeriod>()
                .HasData(
                    new WebPricePeriod
                    {
                        Id = 1,
                        Name = "WebPricePeriodName1",
                        DateBgn = new DateTime(dtNow.Year, 1, 1),
                        DateEnd = new DateTime(dtNow.Year, 12, 31),
                        Note = "WebPricePeriodNote1",
                        NoteNext = "WebPricePeriodNoteNext1"
                    });

            modelBuilder.Entity<WebPriceBody>()
                .HasData(
                    new WebPriceBody
                    {
                        Id = 1,
                        PeriodId = 1,
                        OrderNum = 1,
                        Name = "WebPriceBodyName1",
                        Measurment = "WebPriceBodyMeasurment1",
                        Price = 1150.0000m
                    },
                    new WebPriceBody
                    {
                        Id = 2,
                        PeriodId = 1,
                        OrderNum = 2,
                        Name = "WebPriceBodyName2",
                        Measurment = "WebPriceBodyMeasurment2",
                        Price = 2875.0000m
                    },
                    new WebPriceBody
                    {
                        Id = 3,
                        PeriodId = 1,
                        OrderNum = 3,
                        Name = "WebPriceBodyName3",
                        Measurment = "WebPriceBodyMeasurment3",
                        Price = 1150.0000m
                    },
                    new WebPriceBody
                    {
                        Id = 4,
                        PeriodId = 1,
                        OrderNum = 4,
                        Name = "WebPriceBodyName4",
                        Measurment = "WebPriceBodyMeasurment4",
                        Price = 690.0000m
                    },
                    new WebPriceBody
                    {
                        Id = 5,
                        PeriodId = 1,
                        OrderNum = 5,
                        Name = "WebPriceBodyName5",
                        Measurment = "WebPriceBodyMeasurment5",
                        Price = 1440.0000m
                    },
                    new WebPriceBody
                    {
                        Id = 6,
                        PeriodId = 1,
                        OrderNum = 6,
                        Name = "WebPriceBodyName6",
                        Measurment = "WebPriceBodyMeasurment6",
                        Price = 500.0000m
                    });

            modelBuilder.Entity<WebPriceBodyNext>()
                .HasData(
                    new WebPriceBodyNext
                    {
                        Id = 1,
                        PeriodId = 1,
                        OrderNum = 1,
                        Title = "WebPriceBodyNextTitle1",
                        Note = "WebPriceBodyNextNote1",
                        Price = 3780.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix1"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 2,
                        PeriodId = 1,
                        OrderNum = 2,
                        Title = "WebPriceBodyNextTitle2",
                        Note = "WebPriceBodyNextNote2",
                        Price = 4930.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix2"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 3,
                        PeriodId = 1,
                        OrderNum = 3,
                        Title = "WebPriceBodyNextTitle3",
                        Note = "WebPriceBodyNextNote3",
                        Price = 5505.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix3"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 4,
                        PeriodId = 1,
                        OrderNum = 4,
                        Title = "WebPriceBodyNextTitle4",
                        Note = "WebPriceBodyNextNote4",
                        Price = 6655.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix4"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 5,
                        PeriodId = 1,
                        OrderNum = 5,
                        Title = "WebPriceBodyNextTitle5",
                        Note = "WebPriceBodyNextNote5",
                        Price = 5620.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix5"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 6,
                        PeriodId = 1,
                        OrderNum = 6,
                        Title = "WebPriceBodyNextTitle6",
                        Note = "WebPriceBodyNextNote6",
                        Price = 7920.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix6"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 7,
                        PeriodId = 1,
                        OrderNum = 7,
                        Title = "WebPriceBodyNextTitle7",
                        Note = "WebPriceBodyNextNote7",
                        Price = 7345.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix7"
                    },
                    new WebPriceBodyNext
                    {
                        Id = 8,
                        PeriodId = 1,
                        OrderNum = 8,
                        Title = "WebPriceBodyNextTitle8",
                        Note = "WebPriceBodyNextNote8",
                        Price = 9645.0000m,
                        PricePrefix = "WebPriceBodyNextPricePrefix8"
                    });


            modelBuilder.Entity<ApplicationLanguageText>()
                .HasData(
                    new ApplicationLanguageText[]
                    {
                        // Contacts ru
                        new ApplicationLanguageText
                        {
                            Id=1,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName1",
                            Value = "Телефон",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=2,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName2",
                            Value = "Телефон (мобильный)",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=3,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName3",
                            Value = "Электронная почта",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=4,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName4",
                            Value = "Скайп",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=5,
                            CreatorUserId = 1,
                            Key = "ContacrNameFull",
                            Value = "Общество с ограниченной ответственностью \"НовТехПром\"",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=6,
                            CreatorUserId = 1,
                            Key = "ContactAddressReg",
                            Value = "241037, Брянская обл, Брянск г, Авиационная ул, дом № 8, квартира 14",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=7,
                            CreatorUserId = 1,
                            Key = "ContactBankName",
                            Value = "ПАО \"БАНК УРАЛСИБ\"",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=8,
                            CreatorUserId = 1,
                            Key = "ContactAddressFact",
                            Value = "241012, Брянская обл, Брянск г, Орловская ул, дом № 23, квартира 46",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=9,
                            CreatorUserId = 1,
                            Key = "ContactNameShort",
                            Value = "ООО \"НовТехПром\"",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=10,
                            CreatorUserId = 1,
                            Key = "ContactAboutCompanyText",
                            Value = "Разработка компьютерных программ и веб-сайтов",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=11,
                            CreatorUserId = 1,
                            Key = "ContactBossName",
                            Value = "Святослав",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=12,
                            CreatorUserId = 1,
                            Key = "ContactBossSurname",
                            Value = "Лавриненко",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=13,
                            CreatorUserId = 1,
                            Key = "ContactBossPatronymic",
                            Value = "Григорьевич",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        // Contacts en
                        new ApplicationLanguageText
                        {
                            Id=14,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName1",
                            Value = "Phone",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=15,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName2",
                            Value = "Phone (mobyle)",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=16,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName3",
                            Value = "Email",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=17,
                            CreatorUserId = 1,
                            Key = "ContactCommunicationName4",
                            Value = "Skype",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=18,
                            CreatorUserId = 1,
                            Key = "ContacrNameFull",
                            Value = "\"Novtehprom\" Limited Trade Development Company",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=19,
                            CreatorUserId = 1,
                            Key = "ContactAddressReg",
                            Value = "Flat 14, 8 Aviatcionaya St, Bryansk, 241037, Russia",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=20,
                            CreatorUserId = 1,
                            Key = "ContactBankName",
                            Value = "PJSC \"BANK URALSIB\"",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=21,
                            CreatorUserId = 1,
                            Key = "ContactAddressFact",
                            Value = "Flat 46, 23 Orlovskaya St, Bryansk, 241012, Russia",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=22,
                            CreatorUserId = 1,
                            Key = "ContactNameShort",
                            Value = "Novtehprom Ltd",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=23,
                            CreatorUserId = 1,
                            Key = "ContactAboutCompanyText",
                            Value = "Computer programs and websites development",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=24,
                            CreatorUserId = 1,
                            Key = "ContactBossName",
                            Value = "Svyatoslav",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=25,
                            CreatorUserId = 1,
                            Key = "ContactBossSurname",
                            Value = "Lavrinenko",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=26,
                            CreatorUserId = 1,
                            Key = "ContactBossPatronymic",
                            Value = " ",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        // WebPricePeriod ru
                        new ApplicationLanguageText
                        {
                            Id=27,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodName1",
                            Value = "Основной",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=28,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodNote1",
                            Value = "",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=29,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodNoteNext1",
                            Value = "",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        // WebPricePeriod en
                        new ApplicationLanguageText
                        {
                            Id=30,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodName1",
                            Value = "Main",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=31,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodNote1",
                            Value = "",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=32,
                            CreatorUserId = 1,
                            Key = "WebPricePeriodNoteNext1",
                            Value = "",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        // WebPriceBody ru
                        new ApplicationLanguageText
                        {
                            Id=33,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName1",
                            Value = "Дизайн 1 страницы без использования покупной графики",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=34,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName2",
                            Value = "Дизайн 1 страницы с использованием покупной графики",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=35,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName3",
                            Value = "Программа управления содержимым для одной страницы",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=36,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName4",
                            Value = "Верстка одной страницы",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=37,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName5",
                            Value = "Хостинг",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=38,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName6",
                            Value = "Регистрация доменного имени",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=39,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment1",
                            Value = "шт.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=40,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment2",
                            Value = "шт.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=41,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment3",
                            Value = "шт.       ",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=42,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment4",
                            Value = "шт.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=43,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment5",
                            Value = "год.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=44,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment6",
                            Value = "год.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        // WebPriceBodyNext ru
                        new ApplicationLanguageText
                        {
                            Id=45,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle1",
                            Value = "Простой, одностраничный сайт",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=46,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle2",
                            Value = "Простой, одностраничный сайт с редактированием",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=47,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle3",
                            Value = "Одностраничный сайт с покупной графикой",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=48,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle4",
                            Value = "Одностраничный сайт с покупной графикой с редактированием",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=49,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle5",
                            Value = "Простой, многостраничный сайт",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=50,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle6",
                            Value = "Простой многостраничный сайт с редактированием",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=51,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle7",
                            Value = "Многостраничный сайт  с покупной графикой",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=52,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle8",
                            Value = "Многостраничный сайт  с покупной графикой и редактированием",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },

                        new ApplicationLanguageText
                        {
                            Id=53,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote1",
                            Value = "Простой, одностраничный сайт (сайт-визитка), без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 3780 р. Из которых 1940р – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=54,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote2",
                            Value = "Простой, одностраничный сайт (сайт-визитка), с возможностью самостоятельно редактировать содержимое, вместе с хостингом и регистрацией доменного имени обойдется вам в 4930р. Из которых 1940р. – ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=55,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote3",
                            Value = "Одностраничный сайт (сайт-визитка), без редактирования, с поиском и покупкой лицензионных изображений, вместе с хостингом и регистрацией доменного имени обойдется вам от 5505р. Из которых 1940р. – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=56,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote4",
                            Value = "Одностраничный сайт (сайт-визитка), с редактированием и с покупкой лицензионных изображений, вместе с хостингом и регистрацией доменного имени обойдется вам в 6655р. Из которых 1940р. – ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=57,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote5",
                            Value = "Простой, многостраничный сайт, с главной страницей и с одним дизайном для страниц содержания, без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 5620р. Из которых 1940р. – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=58,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote6",
                            Value = "Простой, многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания, с одинаковой структурой страниц содержания с возможностью редактировать содержимое самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 7920р. Из которых 1940р. – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=59,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote7",
                            Value = "Многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания,  с одинаковой структурой страниц содержания, с поиском и покупкой лицензионных изображений, без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам от 7345р. Из которых 1940р. – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=60,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote8",
                            Value = "Многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания,  с одинаковой структурой страниц содержания, с поиском и покупкой лицензионных изображений, с возможностью редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам от 9645 р. Из которых 1940р – это ежегодная плата за хостинг и домен.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=61,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix1",
                            Value = " ",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=62,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix2",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=63,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix3",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=64,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix4",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=65,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix5",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=66,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix6",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=67,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix7",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        new ApplicationLanguageText
                        {
                            Id=68,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix8",
                            Value = "от",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageName
                        },
                        // WebPriceBody en
                        new ApplicationLanguageText
                        {
                            Id=69,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName1",
                            Value = "1 page design without using purchased graphics",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=70,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName2",
                            Value = "1 page design using purchased graphics",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=71,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName3",
                            Value = "Content management program for a single page",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=72,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName4",
                            Value = "Develop HTML code of a single page",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=73,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName5",
                            Value = "Hosting",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=74,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyName6",
                            Value = "Domain name registration",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=75,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment1",
                            Value = "part",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=76,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment2",
                            Value = "part",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=77,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment3",
                            Value = "part",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=78,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment4",
                            Value = "part",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=79,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment5",
                            Value = "year",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=80,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyMeasurment6",
                            Value = "year",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        // WebPriceBodyNext en
                        new ApplicationLanguageText
                        {
                            Id=81,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle1",
                            Value = "Simple, one-page website",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=82,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle2",
                            Value = "Simple, one-page site with editing program",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=83,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle3",
                            Value = "One-page site with purchased graphics",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=84,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle4",
                            Value = "One-page site with purchased graphics with editing program",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=85,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle5",
                            Value = "Simple, multi-page website",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=86,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle6",
                            Value = "Simple multi-page site with editing program",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=87,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle7",
                            Value = "Multi-page site with purchased graphics",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=88,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextTitle8",
                            Value = "Multi-page site with purchased graphics and editing program",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=89,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote1",
                            Value = "A simple, one-page site (business card site), without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you 3780 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=90,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote2",
                            Value = "A simple, one-page site (business card site), with the ability to edit the content yourself, along with hosting and domain name registration, will cost you 4930 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=91,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote3",
                            Value = "Oodnostranichnyy site (site-vcard), without editing, with search for and purchase licensed images, together with hosting and registration domain name will cost you from 5505 rubls. Of which 1940r – this annual paid for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=92,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote4",
                            Value = "A single-page website (business card site), with editing and buying licensed images, together with hosting and domain name registration, will cost you 6655 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=93,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote5",
                            Value = "A simple, multi-page site, with a main page and one design for content pages, without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you 5620 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=94,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote6",
                            Value = "A simple, multi-page site, with a main page and a single design for content pages, with the same structure of content pages with the ability to edit the content yourself, together with hosting and domain name registration, will cost you 7920 rubls. of which 1940r is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=95,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote7",
                            Value = "A multi-page site with a main page and one design for content pages, with the same structure of content pages, with search and purchase of licensed images, without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you from 7345 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=96,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextNote8",
                            Value = "A multi-page site, with a main page and one design for content pages, with the same structure of content pages, with search and purchase of licensed images, with the ability to edit the site content yourself, together with hosting and domain name registration, will cost you from 9645 rubles. of which 1940 rubls is an annual fee for hosting and domain.",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=97,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix1",
                            Value = " ",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=98,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix2",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=99,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix3",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=100,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix4",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=101,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix5",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=102,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix6",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=103,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix7",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        },
                        new ApplicationLanguageText
                        {
                            Id=104,
                            CreatorUserId = 1,
                            Key = "WebPriceBodyNextPricePrefix8",
                            Value = "from",
                            Source = NovtehpromConsts.LocalizationSourceName,
                            LanguageName = languageNameEn
                        }
                    });
        }

        /// <summary>
        /// Расширение фукции отмены статуса удаления для внутренных объектов ([Owned]) сущности наследующей тип FullAuditedEntity 
        /// </summary>
        /// <param name="entry"></param>
        protected override void CancelDeletionForSoftDelete(EntityEntry entry)
        {
            if (entry.Entity is IOwnedSoftDelete ownedEntity && ownedEntity.IsSoftDelete)
            {
                entry.State = EntityState.Modified;
            }
            else
            {
                base.CancelDeletionForSoftDelete(entry);
            }
        }
    }
}
