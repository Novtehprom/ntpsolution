﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Novtehprom.Configuration;
using Novtehprom.Web;

namespace Novtehprom.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class NovtehpromDbContextFactory : IDesignTimeDbContextFactory<NovtehpromDbContext>
    {
        public NovtehpromDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<NovtehpromDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            NovtehpromDbContextConfigurer.Configure(builder, configuration.GetConnectionString(NovtehpromConsts.ConnectionStringName));

            return new NovtehpromDbContext(builder.Options);
        }
    }
}
