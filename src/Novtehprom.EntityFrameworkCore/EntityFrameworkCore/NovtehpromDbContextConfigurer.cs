using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Novtehprom.EntityFrameworkCore
{
    public static class NovtehpromDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<NovtehpromDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<NovtehpromDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
