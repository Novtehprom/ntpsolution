﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novtehprom.Migrations
{
    public partial class ContactsRemBoss : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact_BossName",
                table: "Contacts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Contact_BossName",
                table: "Contacts",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }
    }
}
