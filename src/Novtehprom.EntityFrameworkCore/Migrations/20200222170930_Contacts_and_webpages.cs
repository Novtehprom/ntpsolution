﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novtehprom.Migrations
{
    public partial class Contacts_and_webpages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BamkName",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "BuhName",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "BuhPatronymic",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "BuhSurname",
                table: "Contacts");

            migrationBuilder.AlterColumn<string>(
                name: "NoteNext",
                table: "WebPricePeriods",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "WebPricePeriods",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebPricePeriods",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "WebPriceBodyNexts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PricePrefix",
                table: "WebPriceBodyNexts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "WebPriceBodyNexts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebPriceBodies",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Measurment",
                table: "WebPriceBodies",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossSurname",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(25)",
                oldMaxLength: 25,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossPatronymic",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldMaxLength: 30,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossName",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NameShort",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NameFull",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AddressReg",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AddressFact",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AboutCompanyText",
                table: "Contacts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankName",
                table: "Contacts",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserNameOrEmailAddress",
                table: "AbpUserLoginAttempts",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "AbpSettings",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(2000)",
                oldMaxLength: 2000,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Communication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrderNum = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Value = table.Column<string>(maxLength: 256, nullable: true),
                    ContactId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Communication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Communication_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AbpLanguageTexts",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "Key", "LanguageName", "LastModificationTime", "LastModifierUserId", "Source", "TenantId", "Value" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 2, 22, 20, 9, 27, 811, DateTimeKind.Local).AddTicks(2382), 1L, "ContactCommunicationName1", "ru", null, null, "Novtehprom", null, "Телефон" },
                    { 77L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1168), 1L, "WebPriceBodyMeasurment3", "en", null, null, "Novtehprom", null, "part" },
                    { 76L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1158), 1L, "WebPriceBodyMeasurment2", "en", null, null, "Novtehprom", null, "part" },
                    { 75L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1148), 1L, "WebPriceBodyMeasurment1", "en", null, null, "Novtehprom", null, "part" },
                    { 74L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1138), 1L, "WebPriceBodyName6", "en", null, null, "Novtehprom", null, "Domain name registration" },
                    { 73L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1128), 1L, "WebPriceBodyName5", "en", null, null, "Novtehprom", null, "Hosting" },
                    { 72L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1118), 1L, "WebPriceBodyName4", "en", null, null, "Novtehprom", null, "Develop HTML code of a single page" },
                    { 71L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1108), 1L, "WebPriceBodyName3", "en", null, null, "Novtehprom", null, "Content management program for a single page" },
                    { 70L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1083), 1L, "WebPriceBodyName2", "en", null, null, "Novtehprom", null, "1 page design using purchased graphics" },
                    { 69L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1073), 1L, "WebPriceBodyName1", "en", null, null, "Novtehprom", null, "1 page design without using purchased graphics" },
                    { 68L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1063), 1L, "WebPriceBodyNextPricePrefix8", "ru", null, null, "Novtehprom", null, "от" },
                    { 78L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1178), 1L, "WebPriceBodyMeasurment4", "en", null, null, "Novtehprom", null, "part" },
                    { 67L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1053), 1L, "WebPriceBodyNextPricePrefix7", "ru", null, null, "Novtehprom", null, "от" },
                    { 65L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1032), 1L, "WebPriceBodyNextPricePrefix5", "ru", null, null, "Novtehprom", null, "от" },
                    { 64L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1022), 1L, "WebPriceBodyNextPricePrefix4", "ru", null, null, "Novtehprom", null, "от" },
                    { 63L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1012), 1L, "WebPriceBodyNextPricePrefix3", "ru", null, null, "Novtehprom", null, "от" },
                    { 62L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1002), 1L, "WebPriceBodyNextPricePrefix2", "ru", null, null, "Novtehprom", null, "от" },
                    { 61L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(992), 1L, "WebPriceBodyNextPricePrefix1", "ru", null, null, "Novtehprom", null, " " },
                    { 60L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(982), 1L, "WebPriceBodyNextNote8", "ru", null, null, "Novtehprom", null, "Многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания,  с одинаковой структурой страниц содержания, с поиском и покупкой лицензионных изображений, с возможностью редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам от 9645 р. Из которых 1940р – это ежегодная плата за хостинг и домен." },
                    { 59L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(972), 1L, "WebPriceBodyNextNote7", "ru", null, null, "Novtehprom", null, "Многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания,  с одинаковой структурой страниц содержания, с поиском и покупкой лицензионных изображений, без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам от 7345р. Из которых 1940р. – это ежегодная плата за хостинг и домен." },
                    { 58L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(962), 1L, "WebPriceBodyNextNote6", "ru", null, null, "Novtehprom", null, "Простой, многостраничный сайт, с главной страницей и с одним дизайном для  страниц содержания, с одинаковой структурой страниц содержания с возможностью редактировать содержимое самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 7920р. Из которых 1940р. – это ежегодная плата за хостинг и домен." },
                    { 57L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(951), 1L, "WebPriceBodyNextNote5", "ru", null, null, "Novtehprom", null, "Простой, многостраничный сайт, с главной страницей и с одним дизайном для страниц содержания, без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 5620р. Из которых 1940р. – это ежегодная плата за хостинг и домен." },
                    { 56L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(941), 1L, "WebPriceBodyNextNote4", "ru", null, null, "Novtehprom", null, "Одностраничный сайт (сайт-визитка), с редактированием и с покупкой лицензионных изображений, вместе с хостингом и регистрацией доменного имени обойдется вам в 6655р. Из которых 1940р. – ежегодная плата за хостинг и домен." },
                    { 66L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1043), 1L, "WebPriceBodyNextPricePrefix6", "ru", null, null, "Novtehprom", null, "от" },
                    { 79L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1189), 1L, "WebPriceBodyMeasurment5", "en", null, null, "Novtehprom", null, "year" },
                    { 80L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1199), 1L, "WebPriceBodyMeasurment6", "en", null, null, "Novtehprom", null, "year" },
                    { 81L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1209), 1L, "WebPriceBodyNextTitle1", "en", null, null, "Novtehprom", null, "Simple, one-page website" },
                    { 104L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1443), 1L, "WebPriceBodyNextPricePrefix8", "en", null, null, "Novtehprom", null, "from" },
                    { 103L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1432), 1L, "WebPriceBodyNextPricePrefix7", "en", null, null, "Novtehprom", null, "from" },
                    { 102L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1422), 1L, "WebPriceBodyNextPricePrefix6", "en", null, null, "Novtehprom", null, "from" },
                    { 101L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1412), 1L, "WebPriceBodyNextPricePrefix5", "en", null, null, "Novtehprom", null, "from" },
                    { 100L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1402), 1L, "WebPriceBodyNextPricePrefix4", "en", null, null, "Novtehprom", null, "from" },
                    { 99L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1392), 1L, "WebPriceBodyNextPricePrefix3", "en", null, null, "Novtehprom", null, "from" },
                    { 98L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1382), 1L, "WebPriceBodyNextPricePrefix2", "en", null, null, "Novtehprom", null, "from" },
                    { 97L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1371), 1L, "WebPriceBodyNextPricePrefix1", "en", null, null, "Novtehprom", null, " " },
                    { 96L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1361), 1L, "WebPriceBodyNextNote8", "en", null, null, "Novtehprom", null, "A multi-page site, with a main page and one design for content pages, with the same structure of content pages, with search and purchase of licensed images, with the ability to edit the site content yourself, together with hosting and domain name registration, will cost you from 9645 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 95L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1350), 1L, "WebPriceBodyNextNote7", "en", null, null, "Novtehprom", null, "A multi-page site with a main page and one design for content pages, with the same structure of content pages, with search and purchase of licensed images, without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you from 7345 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 94L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1339), 1L, "WebPriceBodyNextNote6", "en", null, null, "Novtehprom", null, "A simple, multi-page site, with a main page and a single design for content pages, with the same structure of content pages with the ability to edit the content yourself, together with hosting and domain name registration, will cost you 7920 rubls. of which 1940r is an annual fee for hosting and domain." },
                    { 93L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1329), 1L, "WebPriceBodyNextNote5", "en", null, null, "Novtehprom", null, "A simple, multi-page site, with a main page and one design for content pages, without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you 5620 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 92L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1319), 1L, "WebPriceBodyNextNote4", "en", null, null, "Novtehprom", null, "A single-page website (business card site), with editing and buying licensed images, together with hosting and domain name registration, will cost you 6655 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 91L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1309), 1L, "WebPriceBodyNextNote3", "en", null, null, "Novtehprom", null, "Oodnostranichnyy site (site-vcard), without editing, with search for and purchase licensed images, together with hosting and registration domain name will cost you from 5505 rubls. Of which 1940r – this annual paid for hosting and domain." },
                    { 90L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1298), 1L, "WebPriceBodyNextNote2", "en", null, null, "Novtehprom", null, "A simple, one-page site (business card site), with the ability to edit the content yourself, along with hosting and domain name registration, will cost you 4930 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 89L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1288), 1L, "WebPriceBodyNextNote1", "en", null, null, "Novtehprom", null, "A simple, one-page site (business card site), without the ability to edit the site content yourself, together with hosting and domain name registration, will cost you 3780 rubles. of which 1940 rubls is an annual fee for hosting and domain." },
                    { 88L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1278), 1L, "WebPriceBodyNextTitle8", "en", null, null, "Novtehprom", null, "Multi-page site with purchased graphics and editing program" },
                    { 87L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1268), 1L, "WebPriceBodyNextTitle7", "en", null, null, "Novtehprom", null, "Multi-page site with purchased graphics" },
                    { 86L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1258), 1L, "WebPriceBodyNextTitle6", "en", null, null, "Novtehprom", null, "Simple multi-page site with editing program" },
                    { 85L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1249), 1L, "WebPriceBodyNextTitle5", "en", null, null, "Novtehprom", null, "Simple, multi-page website" },
                    { 84L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1239), 1L, "WebPriceBodyNextTitle4", "en", null, null, "Novtehprom", null, "One-page site with purchased graphics with editing program" },
                    { 83L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1229), 1L, "WebPriceBodyNextTitle3", "en", null, null, "Novtehprom", null, "One-page site with purchased graphics" },
                    { 82L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(1219), 1L, "WebPriceBodyNextTitle2", "en", null, null, "Novtehprom", null, "Simple, one-page site with editing program" },
                    { 55L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(931), 1L, "WebPriceBodyNextNote3", "ru", null, null, "Novtehprom", null, "Одностраничный сайт (сайт-визитка), без редактирования, с поиском и покупкой лицензионных изображений, вместе с хостингом и регистрацией доменного имени обойдется вам от 5505р. Из которых 1940р. – это ежегодная плата за хостинг и домен." },
                    { 54L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(921), 1L, "WebPriceBodyNextNote2", "ru", null, null, "Novtehprom", null, "Простой, одностраничный сайт (сайт-визитка), с возможностью самостоятельно редактировать содержимое, вместе с хостингом и регистрацией доменного имени обойдется вам в 4930р. Из которых 1940р. – ежегодная плата за хостинг и домен." },
                    { 53L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(909), 1L, "WebPriceBodyNextNote1", "ru", null, null, "Novtehprom", null, "Простой, одностраничный сайт (сайт-визитка), без возможности редактировать содержимое сайта самостоятельно, вместе с хостингом и регистрацией доменного имени обойдется вам в 3780 р. Из которых 1940р – это ежегодная плата за хостинг и домен." },
                    { 52L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(899), 1L, "WebPriceBodyNextTitle8", "ru", null, null, "Novtehprom", null, "Многостраничный сайт  с покупной графикой и редактированием" },
                    { 23L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(599), 1L, "ContactAboutCompanyText", "en", null, null, "Novtehprom", null, "Computer programs and websites development" },
                    { 22L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(589), 1L, "ContactNameShort", "en", null, null, "Novtehprom", null, "Novtehprom Ltd" },
                    { 21L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(579), 1L, "ContactAddressFact", "en", null, null, "Novtehprom", null, "Flat 46, 23 Orlovskaya St, Bryansk, 241012, Russia" },
                    { 20L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(569), 1L, "ContactBankName", "en", null, null, "Novtehprom", null, "PJSC \"BANK URALSIB\"" },
                    { 19L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(559), 1L, "ContactAddressReg", "en", null, null, "Novtehprom", null, "Flat 14, 8 Aviatcionaya St, Bryansk, 241037, Russia" },
                    { 18L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(549), 1L, "ContacrNameFull", "en", null, null, "Novtehprom", null, "\"Novtehprom\" Limited Trade Development Company" },
                    { 17L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(539), 1L, "ContactCommunicationName4", "en", null, null, "Novtehprom", null, "Skype" },
                    { 16L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(529), 1L, "ContactCommunicationName3", "en", null, null, "Novtehprom", null, "Email" },
                    { 15L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(518), 1L, "ContactCommunicationName2", "en", null, null, "Novtehprom", null, "Phone (mobyle)" },
                    { 14L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(508), 1L, "ContactCommunicationName1", "en", null, null, "Novtehprom", null, "Phone" },
                    { 24L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(609), 1L, "ContactBossName", "en", null, null, "Novtehprom", null, "Svyatoslav" },
                    { 13L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(498), 1L, "ContactBossPatronymic", "ru", null, null, "Novtehprom", null, "Григорьевич" },
                    { 11L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(478), 1L, "ContactBossName", "ru", null, null, "Novtehprom", null, "Святослав" },
                    { 10L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(467), 1L, "ContactAboutCompanyText", "ru", null, null, "Novtehprom", null, "Разработка компьютерных программ и веб-сайтов" },
                    { 9L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(457), 1L, "ContactNameShort", "ru", null, null, "Novtehprom", null, "ООО \"НовТехПром\"" },
                    { 8L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(446), 1L, "ContactAddressFact", "ru", null, null, "Novtehprom", null, "241012, Брянская обл, Брянск г, Орловская ул, дом № 23, квартира 46" },
                    { 7L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(436), 1L, "ContactBankName", "ru", null, null, "Novtehprom", null, "ПАО \"БАНК УРАЛСИБ\"" },
                    { 6L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(426), 1L, "ContactAddressReg", "ru", null, null, "Novtehprom", null, "241037, Брянская обл, Брянск г, Авиационная ул, дом № 8, квартира 14" },
                    { 5L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(416), 1L, "ContacrNameFull", "ru", null, null, "Novtehprom", null, "Общество с ограниченной ответственностью \"НовТехПром\"" },
                    { 4L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(402), 1L, "ContactCommunicationName4", "ru", null, null, "Novtehprom", null, "Скайп" },
                    { 3L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(391), 1L, "ContactCommunicationName3", "ru", null, null, "Novtehprom", null, "Электронная почта" },
                    { 2L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(321), 1L, "ContactCommunicationName2", "ru", null, null, "Novtehprom", null, "Телефон (мобильный)" },
                    { 12L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(488), 1L, "ContactBossSurname", "ru", null, null, "Novtehprom", null, "Лавриненко" },
                    { 25L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(619), 1L, "ContactBossSurname", "en", null, null, "Novtehprom", null, "Lavrinenko" },
                    { 26L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(629), 1L, "ContactBossPatronymic", "en", null, null, "Novtehprom", null, " " },
                    { 40L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(777), 1L, "WebPriceBodyMeasurment2", "ru", null, null, "Novtehprom", null, "шт." },
                    { 49L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(869), 1L, "WebPriceBodyNextTitle5", "ru", null, null, "Novtehprom", null, "Простой, многостраничный сайт" },
                    { 48L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(859), 1L, "WebPriceBodyNextTitle4", "ru", null, null, "Novtehprom", null, "Одностраничный сайт с покупной графикой с редактированием" },
                    { 47L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(849), 1L, "WebPriceBodyNextTitle3", "ru", null, null, "Novtehprom", null, "Одностраничный сайт с покупной графикой" },
                    { 46L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(839), 1L, "WebPriceBodyNextTitle2", "ru", null, null, "Novtehprom", null, "Простой, одностраничный сайт с редактированием" },
                    { 45L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(829), 1L, "WebPriceBodyNextTitle1", "ru", null, null, "Novtehprom", null, "Простой, одностраничный сайт" },
                    { 44L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(819), 1L, "WebPriceBodyMeasurment6", "ru", null, null, "Novtehprom", null, "год." },
                    { 43L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(809), 1L, "WebPriceBodyMeasurment5", "ru", null, null, "Novtehprom", null, "год." },
                    { 42L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(799), 1L, "WebPriceBodyMeasurment4", "ru", null, null, "Novtehprom", null, "шт." },
                    { 41L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(789), 1L, "WebPriceBodyMeasurment3", "ru", null, null, "Novtehprom", null, "шт.       " },
                    { 27L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(639), 1L, "WebPricePeriodName1", "ru", null, null, "Novtehprom", null, "Основной" },
                    { 50L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(879), 1L, "WebPriceBodyNextTitle6", "ru", null, null, "Novtehprom", null, "Простой многостраничный сайт с редактированием" },
                    { 39L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(763), 1L, "WebPriceBodyMeasurment1", "ru", null, null, "Novtehprom", null, "шт." },
                    { 37L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(743), 1L, "WebPriceBodyName5", "ru", null, null, "Novtehprom", null, "Хостинг" },
                    { 36L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(733), 1L, "WebPriceBodyName4", "ru", null, null, "Novtehprom", null, "Верстка одной страницы" },
                    { 35L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(722), 1L, "WebPriceBodyName3", "ru", null, null, "Novtehprom", null, "Программа управления содержимым для одной страницы" },
                    { 34L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(712), 1L, "WebPriceBodyName2", "ru", null, null, "Novtehprom", null, "Дизайн 1 страницы с использованием покупной графики" },
                    { 33L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(702), 1L, "WebPriceBodyName1", "ru", null, null, "Novtehprom", null, "Дизайн 1 страницы без использования покупной графики" },
                    { 32L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(692), 1L, "WebPricePeriodNoteNext1", "en", null, null, "Novtehprom", null, "" },
                    { 31L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(682), 1L, "WebPricePeriodNote1", "en", null, null, "Novtehprom", null, "" },
                    { 30L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(672), 1L, "WebPricePeriodName1", "en", null, null, "Novtehprom", null, "Main" },
                    { 29L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(662), 1L, "WebPricePeriodNoteNext1", "ru", null, null, "Novtehprom", null, "" },
                    { 28L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(652), 1L, "WebPricePeriodNote1", "ru", null, null, "Novtehprom", null, "" },
                    { 38L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(753), 1L, "WebPriceBodyName6", "ru", null, null, "Novtehprom", null, "Регистрация доменного имени" },
                    { 51L, new DateTime(2020, 2, 22, 20, 9, 27, 812, DateTimeKind.Local).AddTicks(889), 1L, "WebPriceBodyNextTitle7", "ru", null, null, "Novtehprom", null, "Многостраничный сайт  с покупной графикой" }
                });

            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "AboutCompanyText", "Account", "AddressFact", "AddressReg", "BainkBIK", "BankName", "CorrAccount", "CreationTime", "CreatorUserId", "DeleterUserId", "DeletionTime", "INN", "IsDeleted", "KPP", "LastModificationTime", "LastModifierUserId", "NameFull", "NameShort", "OGRN", "OKPO", "OKVED", "BossName", "BossPatronymic", "BossSurname" },
                values: new object[] { 1, "ContactAboutCompanyText", "40702810602380000513", "ContactAddressFact", "ContactAddressReg", "044525787", "ContactBankName", "30101810100000000787", new DateTime(2020, 2, 22, 20, 9, 27, 798, DateTimeKind.Local).AddTicks(8121), 1L, null, null, "3234045475", false, "325701001", null, null, "ContacrNameFull", "ContactNameShort", "1023202747622", "57332029", "51.53.24", "ContactBossName", "ContactBossPatronymic", "ContactBossSurname" });

            migrationBuilder.InsertData(
                table: "WebPricePeriods",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "DateBgn", "DateEnd", "DeleterUserId", "DeletionTime", "IsDeleted", "LastModificationTime", "LastModifierUserId", "Name", "Note", "NoteNext" },
                values: new object[] { 1, new DateTime(2020, 2, 22, 20, 9, 27, 808, DateTimeKind.Local).AddTicks(4801), null, new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "WebPricePeriodName1", "WebPricePeriodNote1", "WebPricePeriodNoteNext1" });

            migrationBuilder.InsertData(
                table: "Communication",
                columns: new[] { "Id", "ContactId", "CreationTime", "CreatorUserId", "DeleterUserId", "DeletionTime", "IsDeleted", "LastModificationTime", "LastModifierUserId", "Name", "OrderNum", "Value" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 2, 22, 20, 9, 27, 807, DateTimeKind.Local).AddTicks(7698), 1L, null, null, false, null, null, "ContactCommunicationName1", 1, "8(483)2-571337" },
                    { 2, 1, new DateTime(2020, 2, 22, 20, 9, 27, 808, DateTimeKind.Local).AddTicks(1905), 1L, null, null, false, null, null, "ContactCommunicationName2", 2, "+7(910)330-68-20" },
                    { 3, 1, new DateTime(2020, 2, 22, 20, 9, 27, 808, DateTimeKind.Local).AddTicks(1921), 1L, null, null, false, null, null, "ContactCommunicationName3", 3, "novtehprom@yandex.ru" },
                    { 4, 1, new DateTime(2020, 2, 22, 20, 9, 27, 808, DateTimeKind.Local).AddTicks(1926), 1L, null, null, false, null, null, "ContactCommunicationName4", 4, "lavlav281" }
                });

            migrationBuilder.InsertData(
                table: "WebPriceBodies",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "DeleterUserId", "DeletionTime", "IsDeleted", "LastModificationTime", "LastModifierUserId", "Measurment", "Name", "OrderNum", "PeriodId", "Price" },
                values: new object[,]
                {
                    { 6, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(8087), null, null, null, false, null, null, "WebPriceBodyMeasurment6", "WebPriceBodyName6", 6, 1, 500.0000m },
                    { 4, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(8077), null, null, null, false, null, null, "WebPriceBodyMeasurment4", "WebPriceBodyName4", 4, 1, 690.0000m },
                    { 5, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(8082), null, null, null, false, null, null, "WebPriceBodyMeasurment5", "WebPriceBodyName5", 5, 1, 1440.0000m },
                    { 2, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(8043), null, null, null, false, null, null, "WebPriceBodyMeasurment2", "WebPriceBodyName2", 2, 1, 2875.0000m },
                    { 1, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(2589), null, null, null, false, null, null, "WebPriceBodyMeasurment1", "WebPriceBodyName1", 1, 1, 1150.0000m },
                    { 3, new DateTime(2020, 2, 22, 20, 9, 27, 809, DateTimeKind.Local).AddTicks(8071), null, null, null, false, null, null, "WebPriceBodyMeasurment3", "WebPriceBodyName3", 3, 1, 1150.0000m }
                });

            migrationBuilder.InsertData(
                table: "WebPriceBodyNexts",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "DeleterUserId", "DeletionTime", "IsDeleted", "LastModificationTime", "LastModifierUserId", "Note", "OrderNum", "PeriodId", "Price", "PricePrefix", "Title" },
                values: new object[,]
                {
                    { 7, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6591), null, null, null, false, null, null, "WebPriceBodyNextNote7", 7, 1, 7345.0000m, "WebPriceBodyNextPricePrefix7", "WebPriceBodyNextTitle7" },
                    { 1, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(41), null, null, null, false, null, null, "WebPriceBodyNextNote1", 1, 1, 3780.0000m, "WebPriceBodyNextPricePrefix1", "WebPriceBodyNextTitle1" },
                    { 2, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6540), null, null, null, false, null, null, "WebPriceBodyNextNote2", 2, 1, 4930.0000m, "WebPriceBodyNextPricePrefix2", "WebPriceBodyNextTitle2" },
                    { 3, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6573), null, null, null, false, null, null, "WebPriceBodyNextNote3", 3, 1, 5505.0000m, "WebPriceBodyNextPricePrefix3", "WebPriceBodyNextTitle3" },
                    { 4, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6578), null, null, null, false, null, null, "WebPriceBodyNextNote4", 4, 1, 6655.0000m, "WebPriceBodyNextPricePrefix4", "WebPriceBodyNextTitle4" },
                    { 5, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6582), null, null, null, false, null, null, "WebPriceBodyNextNote5", 5, 1, 5620.0000m, "WebPriceBodyNextPricePrefix5", "WebPriceBodyNextTitle5" },
                    { 6, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6587), null, null, null, false, null, null, "WebPriceBodyNextNote6", 6, 1, 7920.0000m, "WebPriceBodyNextPricePrefix6", "WebPriceBodyNextTitle6" },
                    { 8, new DateTime(2020, 2, 22, 20, 9, 27, 810, DateTimeKind.Local).AddTicks(6595), null, null, null, false, null, null, "WebPriceBodyNextNote8", 8, 1, 9645.0000m, "WebPriceBodyNextPricePrefix8", "WebPriceBodyNextTitle8" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Communication_ContactId",
                table: "Communication",
                column: "ContactId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Communication");

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 18L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 19L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 20L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 21L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 22L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 23L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 24L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 25L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 26L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 27L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 28L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 29L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 30L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 31L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 32L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 33L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 34L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 35L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 36L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 37L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 38L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 39L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 40L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 41L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 42L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 43L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 44L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 45L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 46L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 47L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 48L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 49L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 50L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 51L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 52L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 53L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 54L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 55L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 56L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 57L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 58L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 59L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 60L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 61L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 62L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 63L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 64L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 65L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 66L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 67L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 68L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 69L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 70L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 71L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 72L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 73L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 74L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 75L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 76L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 77L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 78L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 79L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 80L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 81L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 82L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 83L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 84L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 85L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 86L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 87L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 88L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 89L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 90L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 91L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 92L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 93L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 94L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 95L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 96L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 97L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 98L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 99L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 100L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 101L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 102L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 103L);

            migrationBuilder.DeleteData(
                table: "AbpLanguageTexts",
                keyColumn: "Id",
                keyValue: 104L);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "WebPriceBodies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "WebPriceBodyNexts",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "WebPricePeriods",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "BankName",
                table: "Contacts");

            migrationBuilder.AlterColumn<string>(
                name: "NoteNext",
                table: "WebPricePeriods",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "WebPricePeriods",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebPricePeriods",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "WebPriceBodyNexts",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PricePrefix",
                table: "WebPriceBodyNexts",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "WebPriceBodyNexts",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebPriceBodies",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Measurment",
                table: "WebPriceBodies",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NameShort",
                table: "Contacts",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NameFull",
                table: "Contacts",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AddressReg",
                table: "Contacts",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AddressFact",
                table: "Contacts",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AboutCompanyText",
                table: "Contacts",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossSurname",
                table: "Contacts",
                type: "nvarchar(25)",
                maxLength: 25,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossPatronymic",
                table: "Contacts",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BossName",
                table: "Contacts",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BamkName",
                table: "Contacts",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Contacts",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Contacts",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BuhName",
                table: "Contacts",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BuhPatronymic",
                table: "Contacts",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BuhSurname",
                table: "Contacts",
                type: "nvarchar(25)",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserNameOrEmailAddress",
                table: "AbpUserLoginAttempts",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "AbpSettings",
                type: "nvarchar(2000)",
                maxLength: 2000,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
