﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novtehprom.Migrations
{
    public partial class contacts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    NameFull = table.Column<string>(maxLength: 300, nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(maxLength: 150, nullable: true),
                    AddressReg = table.Column<string>(maxLength: 300, nullable: true),
                    INN = table.Column<string>(maxLength: 10, nullable: true),
                    KPP = table.Column<string>(maxLength: 9, nullable: true),
                    BamkName = table.Column<string>(maxLength: 300, nullable: true),
                    Account = table.Column<string>(maxLength: 20, nullable: true),
                    CorrAccount = table.Column<string>(maxLength: 20, nullable: true),
                    BainkBIK = table.Column<string>(maxLength: 9, nullable: true),
                    AddressFact = table.Column<string>(maxLength: 300, nullable: true),
                    Contact_BossName = table.Column<string>(maxLength: 150, nullable: true),
                    OGRN = table.Column<string>(maxLength: 13, nullable: true),
                    OKPO = table.Column<string>(maxLength: 8, nullable: true),
                    OKVED = table.Column<string>(maxLength: 50, nullable: true),
                    NameShort = table.Column<string>(maxLength: 300, nullable: true),
                    AboutCompanyText = table.Column<string>(nullable: true),
                    BossName = table.Column<string>(maxLength: 20, nullable: true),
                    BossSurname = table.Column<string>(maxLength: 25, nullable: true),
                    BossPatronymic = table.Column<string>(maxLength: 30, nullable: true),
                    BuhName = table.Column<string>(maxLength: 20, nullable: true),
                    BuhSurname = table.Column<string>(maxLength: 25, nullable: true),
                    BuhPatronymic = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebPricePeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DateBgn = table.Column<DateTime>(nullable: false),
                    DateEnd = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    Note = table.Column<string>(nullable: true),
                    NoteNext = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPricePeriods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebPriceBodies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 250, nullable: true),
                    OrderNum = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Measurment = table.Column<string>(maxLength: 10, nullable: true),
                    PeriodId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPriceBodies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebPriceBodies_WebPricePeriods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "WebPricePeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WebPriceBodyNexts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 150, nullable: true),
                    OrderNum = table.Column<int>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    PricePrefix = table.Column<string>(maxLength: 10, nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    PeriodId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPriceBodyNexts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebPriceBodyNexts_WebPricePeriods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "WebPricePeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WebPriceBodies_PeriodId",
                table: "WebPriceBodies",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_WebPriceBodyNexts_PeriodId",
                table: "WebPriceBodyNexts",
                column: "PeriodId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "WebPriceBodies");

            migrationBuilder.DropTable(
                name: "WebPriceBodyNexts");

            migrationBuilder.DropTable(
                name: "WebPricePeriods");
        }
    }
}
