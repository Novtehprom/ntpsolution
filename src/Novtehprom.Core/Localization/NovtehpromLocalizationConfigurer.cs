﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Localization;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace Novtehprom.Localization
{
    public static class NovtehpromLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration, IIocManager ioManager)
        {
            localizationConfiguration.Sources.Add(
                new MultiTenantLocalizationSource(
                    NovtehpromConsts.LocalizationSourceName,
                    new MultiTenantLocalizationDictionaryProvider(
                        new XmlEmbeddedFileLocalizationDictionaryProvider(
                            typeof(NovtehpromLocalizationConfigurer).GetAssembly(),
                            "Novtehprom.Localization.SourceFiles"
                        ),
                        ioManager
                    )
                )
            );
        }
    }
}