﻿namespace Novtehprom
{
    public class NovtehpromConsts
    {
        public const string LocalizationSourceName = "Novtehprom";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
