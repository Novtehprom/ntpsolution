﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;

namespace Novtehprom.WebPages.Contacts
{
    /// <summary>
    /// Способ связи
    /// </summary>
    public class Communication : FullAuditedEntity<int>
    {
        /// <summary>
        /// Порядковый номер в списке
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Наименование: телефон, эл-почта, skype и т.п.
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        [MaxLength(256)]
        public string Value { get; set; }
        /// <summary>
        /// Запись контаков
        /// </summary>
        public virtual Contact Contact { get; set; }
        /// <summary>
        /// Идентификатор записи контактов
        /// </summary>
        public int ContactId { get; set; }
    }
}
