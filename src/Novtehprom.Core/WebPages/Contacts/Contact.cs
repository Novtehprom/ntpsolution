﻿using Abp.Domain.Entities.Auditing;
using Novtehprom.WebPages.BaseTypes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Novtehprom.WebPages.Contacts
{
    /// <summary>
    /// Информация о компании. Адрес, р.с телефоны т т.п.
    /// </summary>
    public class Contact : FullAuditedEntity<int>
    {
        /// <summary>
        /// Полное наименование
        /// </summary>
        [MaxLength(256)]
        public string NameFull { get; set; }
        /// <summary>
        /// Телефоны, эл.почта, skype и т.п.
        /// </summary>
        public virtual List<Communication> Communications { get; set; }
        /// <summary>
        /// Юоидический адрес
        /// </summary>
        [MaxLength(256)]
        public string AddressReg { get; set; }
        /// <summary>
        /// ИНН
        /// </summary>
        [MaxLength(10)]
        public string INN { get; set; }
        /// <summary>
        /// КПП
        /// </summary>
        [MaxLength(9)]
        public string KPP { get; set; }
        /// <summary>
        /// Наименование банка
        /// </summary>
        [MaxLength(256)]
        public string BankName { get; set; }
        /// <summary>
        /// Расчетный счет
        /// </summary>
        [MaxLength(20)]
        public string Account { get; set; }
        /// <summary>
        /// Кор счет банка
        /// </summary>
        [MaxLength(20)]
        public string CorrAccount { get; set; }
        /// <summary>
        /// Бик банка
        /// </summary>
        [MaxLength(9)]
        public string BainkBIK { get; set; }
        /// <summary>
        /// Фактический адрес
        /// </summary>
        [MaxLength(256)]
        public string AddressFact { get; set; }
        /// <summary>
        /// ОГРН
        /// </summary>
        [MaxLength(13)]
        public string OGRN { get; set; }
        /// <summary>
        /// ОКПО
        /// </summary>
        [MaxLength(8)]
        public string OKPO { get; set; }
        /// <summary>
        /// ОКВЕД
        /// </summary>
        [MaxLength(50)]
        public string OKVED { get; set; }
        /// <summary>
        /// Краткое наименование
        /// </summary>
        [MaxLength(256)]
        public string NameShort { get; set; }
        /// <summary>
        /// О компании
        /// </summary>
        [MaxLength(256)]
        public string AboutCompanyText { get; set; }
        /// <summary>
        /// Фио директора
        /// </summary>
        public FioFullAudited BossFio { get; set; }
    }
}
