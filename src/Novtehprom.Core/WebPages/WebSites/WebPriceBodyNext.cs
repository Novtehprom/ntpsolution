﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WebPages.WebSites
{
    /// <summary>
    /// Пункт второй части прайса
    /// </summary>
    public class WebPriceBodyNext: FullAuditedEntity<int>
    {
        [MaxLength(256)]
        public string Title { get; set; }
        /// <summary>
        /// Порядковый номер
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Текст
        /// </summary>
        [MaxLength(256)]
        public string Note { get; set; }
        /// <summary>
        /// Префикс для цены, например "от"
        /// </summary>
        [MaxLength(256)]
        public string PricePrefix { get; set; }
        /// <summary>
        /// Цена
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Идентификатор периода
        /// </summary>
        public int PeriodId { get; set; }
        /// <summary>
        /// Период действия прайса
        /// </summary>
        public virtual WebPricePeriod Period { get; set; }
    }
}
