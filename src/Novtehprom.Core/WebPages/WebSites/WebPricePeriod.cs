﻿using Abp.Domain.Entities.Auditing;
using Novtehprom.WebPages.BaseTypes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Novtehprom.WebPages.WebSites
{
    /// <summary>
    /// Период действия прайса на создание сайтов.
    /// </summary>
    public class WebPricePeriod: Period
    {
        /// <summary>
        /// Текст для второй части прайса
        /// </summary>
        [MaxLength(256)]
        public string NoteNext { get; set; }
        /// <summary>
        /// Первая часть прайса
        /// </summary>
        public virtual IList<WebPriceBody> Price { get; set; }
        /// <summary>
        /// Вторая часть прайса
        /// </summary>
        public virtual IList<WebPriceBodyNext> PriceNext { get; set; }

    }
}
