﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;

namespace Novtehprom.WebPages.WebSites
{
    /// <summary>
    /// Пункт первой части прайса
    /// </summary>
    public class WebPriceBody: FullAuditedEntity<int>
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Порядковый номер для сортировки
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Цена 
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// ед. измерения
        /// </summary>
        [MaxLength(256)]
        public string Measurment { get; set; }
        /// <summary>
        /// Иденитфикатор записи периода
        /// </summary>
        public int PeriodId { get; set; }
        /// <summary>
        /// Период действия прайса
        /// </summary>
        public virtual WebPricePeriod Period { get; set; }
    }
}
