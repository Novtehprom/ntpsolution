﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WebPages.BaseTypes
{
    /// <summary>
    /// Базовый клас периода прайсов
    /// </summary>
    public abstract class Period: FullAuditedEntity<int>
    {
        /// <summary>
        /// Начало периода
        /// </summary>
        public DateTime DateBgn { get; set; }
        /// <summary>
        /// Конец периода
        /// </summary>
        public DateTime DateEnd { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Для заметок
        /// </summary>
        [MaxLength(256)]
        public string Note { get; set; }
    }
}
