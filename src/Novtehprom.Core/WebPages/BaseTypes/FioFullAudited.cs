﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novtehprom.WebPages.BaseTypes
{
    /// <summary>
    /// ФИО для обработки магкого удаления при удалении сущностей владельецев
    /// </summary>
    [Owned]
    public class FioFullAudited : Fio, IOwnedSoftDelete
    {
        /// <summary>
        /// Для сущностей наследников FullAuditedEntity - true по умолчанию
        /// </summary>
        [NotMapped]
        public bool IsSoftDelete => true;
    }
}
