﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novtehprom.WebPages.BaseTypes
{
    /// <summary>
    /// Для типов с атрибутом [Owned], принадлежащих сущностям наследникам FullAuditedEntity
    /// </summary>
    public interface IOwnedSoftDelete
    {
        /// <summary>
        /// Для  внутренних сущностей ([Owned]) классов сущностей наследников FullAuditedEntity - установить true  в конструкторе
        /// </summary>
        public bool IsSoftDelete { get; }
    }
}
