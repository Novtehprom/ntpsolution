﻿using Abp.MultiTenancy;
using Novtehprom.Authorization.Users;

namespace Novtehprom.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
