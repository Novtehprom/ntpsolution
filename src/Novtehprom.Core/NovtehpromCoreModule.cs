﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using Novtehprom.Authorization.Roles;
using Novtehprom.Authorization.Users;
using Novtehprom.Configuration;
using Novtehprom.Localization;
using Novtehprom.MultiTenancy;
using Novtehprom.Timing;

namespace Novtehprom
{
    [DependsOn(typeof(AbpZeroCoreModule))]
    public class NovtehpromCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            NovtehpromLocalizationConfigurer.Configure(Configuration.Localization, IocManager);

            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = NovtehpromConsts.MultiTenancyEnabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            Configuration.Settings.Providers.Add<AppSettingProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(NovtehpromCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
            //NovtehpromLocalizationConfigurer.MultiTenantConfigure(Configuration.Localization, IocManager, IocManager.Resolve<IRepository<ApplicationLanguageText, long>>());
        }
    }
}
