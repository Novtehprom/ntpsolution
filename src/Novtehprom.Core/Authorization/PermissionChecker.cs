﻿using Abp.Authorization;
using Novtehprom.Authorization.Roles;
using Novtehprom.Authorization.Users;

namespace Novtehprom.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
