using Abp.AutoMapper;
using Novtehprom.Sessions.Dto;

namespace Novtehprom.Web.Mvc.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
