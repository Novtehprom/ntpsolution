﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Novtehprom.Web.Mvc.Views
{
    public abstract class NovtehpromViewComponent : AbpViewComponent
    {
        protected NovtehpromViewComponent()
        {
            LocalizationSourceName = NovtehpromConsts.LocalizationSourceName;
        }
    }
}
