﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace Novtehprom.Web.Mvc.Views
{
    public abstract class NovtehpromRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected NovtehpromRazorPage()
        {
            LocalizationSourceName = NovtehpromConsts.LocalizationSourceName;
        }
    }
}
