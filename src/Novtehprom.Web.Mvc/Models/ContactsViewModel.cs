﻿using Novtehprom.WebPages.BaseTypes;
using Novtehprom.WepPages.Contacts.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    /// <summary>
    /// Модель представления для страницы контактов
    /// </summary>
    public class ContactsViewModel
    {
        public ContactsViewModel()
        {
            Communications = new List<CommunicationViewModel>();
        }
        public ContactsViewModel(ContactsDto contactsDto)
        {
            NameFull = contactsDto.NameFull;
            AddressReg = contactsDto.AddressReg;
            INN = contactsDto.INN;
            KPP = contactsDto.KPP;
            BankName = contactsDto.BankName;
            Account = contactsDto.Account;
            CorrAccount = contactsDto.CorrAccount;
            BainkBIK = contactsDto.BainkBIK;
            AddressFact = contactsDto.AddressFact;
            OGRN = contactsDto.OGRN;
            OKPO = contactsDto.OKPO;
            OKVED = contactsDto.OKVED;
            NameShort = contactsDto.NameShort;
            AboutCompanyText = contactsDto.AboutCompanyText;
            BossFio = contactsDto.BossFio;
            Communications = new List<CommunicationViewModel>();
            foreach (var comDto in contactsDto.Communications.OrderBy(c => c.OrderNum))
            {
                Communications.Add(new CommunicationViewModel
                {
                    Name = comDto.Name,
                    OrderNum = comDto.OrderNum,
                    Value = comDto.Value
                });
            }
        }
        /// <summary>
        /// Полное наименование
        /// </summary>
        [Required]
        public string NameFull { get; set; }
        /// <summary>
        /// Юоидический адрес
        /// </summary>
        public string AddressReg { get; set; }
        /// <summary>
        /// ИНН
        /// </summary>
        [StringLength(10)]
        [Required]
        public string INN { get; set; }
        /// <summary>
        /// КПП
        /// </summary>
        [StringLength(9)]
        [Required]
        public string KPP { get; set; }
        /// <summary>
        /// Наименование банка
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// Расчетный счет
        /// </summary>
        [StringLength(20)]
        public string Account { get; set; }
        /// <summary>
        /// Кор счет банка
        /// </summary>
        [StringLength(20)]
        public string CorrAccount { get; set; }
        /// <summary>
        /// Бик банка
        /// </summary>
        [StringLength(9)]
        public string BainkBIK { get; set; }
        /// <summary>
        /// Фактический адрес
        /// </summary>
        public string AddressFact { get; set; }
        /// <summary>
        /// ОГРН
        /// </summary>
        [StringLength(13)]
        [Required]
        public string OGRN { get; set; }
        /// <summary>
        /// ОКПО
        /// </summary>
        [StringLength(8)]
        [Required]
        public string OKPO { get; set; }
        /// <summary>
        /// ОКВЕД
        /// </summary>
        [StringLength(50)]
        [Required]
        public string OKVED { get; set; }
        /// <summary>
        /// Краткое наименование
        /// </summary>
        [Required]
        public string NameShort { get; set; }
        /// <summary>
        /// О компании
        /// </summary>
        public string AboutCompanyText { get; set; }
        /// <summary>
        /// Фио директора
        /// </summary>
        public Fio BossFio { get; set; }
        /// <summary>
        /// Способы связи, телефоны, эл почта и т.п
        /// </summary>
        public List<CommunicationViewModel> Communications { get; set; }
        public string Director 
        { 
            get => BossFio.Name + " " + BossFio.Patronymic + " " + BossFio.Surname;
        }
    }
}
