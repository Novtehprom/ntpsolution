﻿using System.Collections.Generic;
using Novtehprom.Roles.Dto;

namespace Novtehprom.Web.Mvc.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}