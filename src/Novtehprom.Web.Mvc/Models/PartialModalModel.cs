﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    /// <summary>
    /// Модель для передачи параметров в частичное предстваление при отображении рекомендательных писем
    /// </summary>
    public class PartialModalModel
    {
        /// <summary>
        /// Идентификатор div для отображения
        /// </summary>
        public string Target { get; set; }
        /// <summary>
        /// Фото документа для отображения
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Заголовок диалогового окна
        /// </summary>
        public string Title { get; set; }
    }
}
