﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    /// <summary>
    /// Модель представления пеовой части прайса
    /// </summary>
    public class WebPriceBodyModel
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Порядковый номер для сортировки
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Цена 
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// ед. измерения
        /// </summary>
        public string Measurment { get; set; }
    }
}
