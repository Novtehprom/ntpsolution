﻿using Novtehprom.WepPages.WebSites.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    public class WebPriceViewModel
    {
        public WebPriceViewModel()
        {

        }
        public WebPriceViewModel(WebPricePeriodDto prDto)
        {
            DateBgn = prDto.DateBgn;
            DateEnd = prDto.DateEnd;
            Name = prDto.Name;
            Note = prDto.Note;
            NoteNext = prDto.NoteNext;
            Price = new List<WebPriceBodyModel>();
            foreach(var pr in prDto.Price.OrderBy(p=>p.OrderNum))
            {
                Price.Add(new WebPriceBodyModel
                {
                    Name = pr.Name,
                    OrderNum = pr.OrderNum,
                    Measurment = pr.Measurment,
                    Price = pr.Price
                });
            }
            PriceNext = new List<WebPriceBodyNextModel>();
            foreach(var prN in prDto.PriceNext.OrderBy(p => p.OrderNum))
            {
                PriceNext.Add(new WebPriceBodyNextModel
                {
                    Note = prN.Note,
                    OrderNum = prN.OrderNum,
                    Price = prN.Price,
                    PricePrefix = prN.PricePrefix,
                    Title = prN.Title
                });
            }
        }

        /// <summary>
        /// Начало периода
        /// </summary>
        public string DateBgn { get; set; }
        /// <summary>
        /// Конец периода
        /// </summary>
        public string DateEnd { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(150)]
        public string Name { get; set; }
        /// <summary>
        /// Для заметок
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Текст для второй части прайса
        /// </summary>
        public string NoteNext { get; set; }
        /// <summary>
        /// Первая часть прайса
        /// </summary>
        public virtual List<WebPriceBodyModel> Price { get; set; }
        /// <summary>
        /// Вторая часть прайса
        /// </summary>
        public virtual List<WebPriceBodyNextModel> PriceNext { get; set; }
    }
}
