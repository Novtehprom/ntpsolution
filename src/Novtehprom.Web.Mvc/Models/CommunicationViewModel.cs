﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    /// <summary>
    /// Модель представления для строки информации о коммуникации
    /// </summary>
    public class CommunicationViewModel
    {
        /// <summary>
        /// Порядковый номер в списке
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Наименование: телефон, эл-почта, skype и т.п.
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        [MaxLength(256)]
        public string Value { get; set; }
    }
}
