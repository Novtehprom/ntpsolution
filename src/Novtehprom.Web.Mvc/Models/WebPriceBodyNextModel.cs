﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.Web.Mvc.Models
{
    /// <summary>
    /// Модель представления второй части прайса
    /// </summary>
    public class WebPriceBodyNextModel
    {
        public string Title { get; set; }
        /// <summary>
        /// Порядковый номер
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Текст
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Префикс для цены, например "от"
        /// </summary>
        public string PricePrefix { get; set; }
        /// <summary>
        /// Цена
        /// </summary>
        public string Price { get; set; }
    }
}
