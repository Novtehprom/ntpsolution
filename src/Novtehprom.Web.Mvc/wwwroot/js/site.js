﻿$(document).ready(function () {
    rightsitebar_activate();
    setTimeout(function () { $('.loader').fadeOut(); }, 50);
});

function rightsitebar_activate() {
    var _this = this;
    var $sidebar = $('#rightsidebar');
    var $navbar = $('.navbar-collapse');

    //Close sidebar
    $(window).click(function (e) {
        var $target = $(e.target);
        if (e.target.nodeName.toLowerCase() === 'i') { $target = $(e.target).parent(); }

        if (!$target.hasClass('js-right-sidebar') && rightsitebar_isopen() && $target.parents('#rightsidebar').length === 0) {
            $sidebar.removeClass('open');
        }
    });

    $('.js-right-sidebar').on('click', function () {
        $sidebar.toggleClass('open');
        if ($("#rightsidebar").hasClass("open")) {
            if ($navbar.hasClass("show")) $navbar.removeClass("show");
        }
    });

}
function rightsitebar_isopen() {
    return $('.right-sidebar').hasClass('open');
}

//Skin tab content set height and show scroll
function setSkinListHeightAndScroll() {
    var height = $(window).height() - ($('.navbar').innerHeight());
    //var height = $(window).height() - ($('#rightsidebar').innerHeight() + $('.right-sidebar').outerHeight());
    var $el = $('#right-navigatin');

    $el.slimScroll({ destroy: true }).height('auto');
    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();

    $el.slimscroll({
        height: height + 'px',
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0',
        disableFadeOut: true
    });
}


(function ($) {

    //Initialize BSB admin features
    $(function () {
        setSkinListHeightAndScroll();
        $(window).resize(function () {
            setSkinListHeightAndScroll();
        });
    });

})(jQuery);
