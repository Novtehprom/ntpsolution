﻿//Skin tab content set height and show scroll
function setSkinListHeightAndScroll() {
    var height = $(window).height() - ($('.navbar').innerHeight());
    //var height = $(window).height() - ($('#rightsidebar').innerHeight() + $('.right-sidebar').outerHeight());
    var $el = $('#right-navigatin');

    $el.slimScroll({ destroy: true }).height('auto');
    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();

    $el.slimscroll({
        height: height + 'px',
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0',
        disableFadeOut: true
    });
}


(function ($) {

    //Initialize BSB admin features
    $(function () {
        setSkinListHeightAndScroll();
        $(window).resize(function () {
            setSkinListHeightAndScroll();
        });
    });

})(jQuery);