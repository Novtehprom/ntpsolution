﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Localization;
using Microsoft.AspNetCore.Mvc;
using Novtehprom.Controllers;
using Novtehprom.Web.Mvc.Models;
using Novtehprom.WepPages.Contacts;

namespace Novtehprom.Web.Mvc.Controllers
{
    /// <summary>
    /// Контроллер для страницы Contacts
    /// </summary>
    public class ContactsController : NovtehpromControllerBase
    {
        public ContactsController(IContactsAppService contactsAppService)
        {
            _contactsAppService = contactsAppService;
        }

        private readonly IContactsAppService _contactsAppService;
        public async Task<IActionResult> IndexAsync()
        {
            var contDto = await _contactsAppService.GetContacts();
            var model = new ContactsViewModel(contDto);
            ViewBag.RetFromLoginUrl = @"/Contacts/Index/";
            return View(model);
        }
    }
}