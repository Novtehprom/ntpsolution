﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Novtehprom.Controllers;
using Novtehprom.Web.Mvc.Models;

namespace Novtehprom.Web.Mvc.Controllers
{
    public class HomeController : NovtehpromControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public IActionResult Index()
        {
            return View();
        }
    }
}
