﻿using Microsoft.AspNetCore.Mvc;
using Novtehprom.Controllers;
using Novtehprom.Web.Mvc.Models;
using Novtehprom.WepPages.WebSites;

namespace Novtehprom.Web.Mvc.Controllers
{
    /// <summary>
    /// Отображение страницы Website
    /// </summary>
    public class WebsiteController : NovtehpromControllerBase
    {
        public WebsiteController(IWebSitesAppService webSitesAppService)
        {
            _webSitesAppService = webSitesAppService;

        }
        private readonly IWebSitesAppService _webSitesAppService;

        public async System.Threading.Tasks.Task<IActionResult> CostAsync()
        {
            var priceDto = await _webSitesAppService.GetPriceAsync();
            var model = new WebPriceViewModel(priceDto);
            ViewBag.RetFromLoginUrl = @"/Website/Cost/";
            return View(model);
        }
    }
}