﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Novtehprom.Controllers;

namespace Novtehprom.Web.Mvc.Controllers
{
    public class ProgramController : NovtehpromControllerBase
    {
        /// <summary>
        /// Стоимость ПО
        /// </summary>
        /// <returns></returns>
        public IActionResult Cost()
        {
            ViewBag.RetFromLoginUrl = @"/Program/Cost/";
            return View();
        }
        /// <summary>
        /// Сроки разработки ПО
        /// </summary>
        /// <returns></returns>
        public IActionResult Timeframe()
        {
            ViewBag.RetFromLoginUrl = @"/Program/Timeframe/";
            return View();
        }

        /// <summary>
        /// Качество разработки ПО
        /// </summary>
        /// <returns></returns>
        public IActionResult Quality()
        {
            ViewBag.RetFromLoginUrl = @"/Program/Quality/";
            return View();
        }

        /// <summary>
        /// Требования к ПО
        /// </summary>
        /// <returns></returns>
        public IActionResult Demands()
        {
            ViewBag.RetFromLoginUrl = @"/Program/Demands/";
            return View();
        }

        /// <summary>
        /// Процесс разработки ПО
        /// </summary>
        /// <returns></returns>
        public IActionResult Implementation()
        {
            ViewBag.RetFromLoginUrl = @"/Program/Implementation/";
            return View();
        }
    }
}