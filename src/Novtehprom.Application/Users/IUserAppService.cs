using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Novtehprom.Roles.Dto;
using Novtehprom.Users.Dto;

namespace Novtehprom.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
