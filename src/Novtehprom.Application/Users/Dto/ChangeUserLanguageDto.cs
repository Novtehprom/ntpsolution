using System.ComponentModel.DataAnnotations;

namespace Novtehprom.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}