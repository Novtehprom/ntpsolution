﻿using Abp.Application.Services.Dto;

namespace Novtehprom.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

