﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Novtehprom.MultiTenancy.Dto;

namespace Novtehprom.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

