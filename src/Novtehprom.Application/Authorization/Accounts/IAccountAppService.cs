﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Novtehprom.Authorization.Accounts.Dto;

namespace Novtehprom.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
