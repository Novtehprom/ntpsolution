﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Novtehprom.Sessions.Dto;

namespace Novtehprom.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
