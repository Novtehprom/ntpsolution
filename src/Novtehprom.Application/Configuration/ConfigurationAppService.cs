﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Novtehprom.Configuration.Dto;

namespace Novtehprom.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : NovtehpromAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
