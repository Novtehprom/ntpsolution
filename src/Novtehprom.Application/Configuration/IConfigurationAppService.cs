﻿using System.Threading.Tasks;
using Novtehprom.Configuration.Dto;

namespace Novtehprom.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
