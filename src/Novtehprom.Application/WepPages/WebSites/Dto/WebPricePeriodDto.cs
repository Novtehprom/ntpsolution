﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WepPages.WebSites.Dto
{
    /// <summary>
    /// Период действия прайса
    /// </summary>
    public class WebPricePeriodDto
    {
        /// <summary>
        /// Начало периода
        /// </summary>
        public string DateBgn { get; set; }
        /// <summary>
        /// Конец периода
        /// </summary>
        public string DateEnd { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(150)]
        public string Name { get; set; }
        /// <summary>
        /// Для заметок
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Текст для второй части прайса
        /// </summary>
        public string NoteNext { get; set; }
        /// <summary>
        /// Первая часть прайса
        /// </summary>
        public virtual List<WebPriceBodyDto> Price { get; set; }
        /// <summary>
        /// Вторая часть прайса
        /// </summary>
        public virtual List<WebPriceBodyNextDto> PriceNext { get; set; }
    }
}
