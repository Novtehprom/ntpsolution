﻿using AutoMapper;
using Novtehprom.WebPages.WebSites;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Novtehprom.WepPages.WebSites.Dto
{
    public class WebPricePeriodMapProfile: Profile
    {
        public WebPricePeriodMapProfile()
        {
            //CreateMap<WebPricePeriodDto, WebPricePeriod>()
            //        .ForMember(c => c.P, opt => opt.MapFrom(m => m.BossFio))
            //        .ForMember(c => c.BuhFio, opt => opt.MapFrom(m => m.BuhFio));
            CreateMap<WebPricePeriod, WebPricePeriodDto>()
                .ForMember(c => c.DateBgn, opt => opt.MapFrom(m => m.DateBgn.ToShortDateString()))
                .ForMember(c => c.DateEnd, opt => opt.MapFrom(m => m.DateEnd.ToShortDateString()))
                .ForMember(c => c.Price, opt => opt.MapFrom(m => m.Price.OrderBy(p => p.OrderNum)
                    .Select(p => new WebPriceBodyDto
                    {
                        OrderNum = p.OrderNum,
                        Name = p.Name,
                        Price = p.Price.ToString("C2", new CultureInfo("ru-RU")),
                        Measurment = p.Measurment
                    })))
                .ForMember(c => c.PriceNext, opt => opt.MapFrom(m => m.PriceNext.OrderBy(p => p.OrderNum)
                    .Select(p => new WebPriceBodyNextDto
                    {
                        OrderNum = p.OrderNum,
                        Note = p.Note,
                        Price = p.Price.ToString("C2", new CultureInfo("ru-RU")),
                        Title = p.Title,
                        PricePrefix = p.PricePrefix
                    })));
        }
    }
}
