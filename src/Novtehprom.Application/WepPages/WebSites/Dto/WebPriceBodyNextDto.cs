﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WepPages.WebSites.Dto
{
    /// <summary>
    /// Строка второй части прайса
    /// </summary>
    public class WebPriceBodyNextDto
    {
        public string Title { get; set; }
        /// <summary>
        /// Порядковый номер
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Текст
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Префикс для цены, например "от"
        /// </summary>
        public string PricePrefix { get; set; }
        /// <summary>
        /// Цена
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// Идентификатор периода
        /// </summary>
        public int PeriodId { get; set; }
        /// <summary>
        /// Период действия прайса
        /// </summary>
        public virtual WebPricePeriodDto Period { get; set; }
    }
}
