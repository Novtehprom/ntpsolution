﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WepPages.WebSites.Dto
{
    public class WebPriceBodyDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Порядковый номер для сортировки
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Цена 
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// ед. измерения
        /// </summary>
        public string Measurment { get; set; }
        /// <summary>
        /// Иденитфикатор записи периода
        /// </summary>
        public int PeriodId { get; set; }
        /// <summary>
        /// Период действия прайса
        /// </summary>
        public virtual WebPricePeriodDto Period { get; set; }
    }
}
