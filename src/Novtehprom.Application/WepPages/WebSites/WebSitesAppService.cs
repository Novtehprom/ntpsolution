﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Novtehprom.WebPages.WebSites;
using Novtehprom.WepPages.WebSites.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.WebSites
{
    public class WebSitesAppService : ApplicationService, IWebSitesAppService
    {
        public WebSitesAppService(IRepository<WebPricePeriod, int> repository)
        {
            this.repository = repository;
        }

        private readonly IRepository<WebPricePeriod, int> repository;

        public async Task<WebPricePeriodDto> GetPriceAsync()
        {
            var datetoday = DateTime.Today;
            var webPricePeriodDto = new WebPricePeriodDto();
            await Task.Factory.StartNew(() =>
            {
                var per = repository.GetAll()
                    .Include(p => p.Price)
                    .Include(p => p.PriceNext)
                    .FirstOrDefault(p => p.DateBgn <= datetoday && p.DateEnd >= datetoday);
                if (per == null)
                {
                    per = repository.GetAll()
                        .OrderByDescending(p => p.DateEnd)
                        .Include(p => p.Price)
                        .Include(p => p.PriceNext)
                        .FirstOrDefault();
                }
                if (per != null)
                {
                    webPricePeriodDto = ObjectMapper.Map<WebPricePeriodDto>(per);
                }
            });
            return webPricePeriodDto;
        }
    }
}
