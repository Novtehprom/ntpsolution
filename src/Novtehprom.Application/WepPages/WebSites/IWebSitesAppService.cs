﻿using Abp.Application.Services;
using Novtehprom.WebPages.Contacts;
using Novtehprom.WepPages.Contacts;
using Novtehprom.WepPages.Contacts.Dto;
using Novtehprom.WepPages.WebSites.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.WebSites
{
    public interface IWebSitesAppService: IApplicationService
    {
        Task<WebPricePeriodDto> GetPriceAsync();
    }
}
