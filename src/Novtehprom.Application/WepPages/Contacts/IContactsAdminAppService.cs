﻿using Abp.Application.Services;
using Novtehprom.WepPages.Contacts.Dto;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.Contacts
{
    /// <summary>
    /// Интерфейс службы, для администрирования контактной информации
    /// </summary>
    public interface IContactsAdminAppService : IAsyncCrudAppService<ContactsDto, int, ContactsDto, ContactsDto, ContactsDto>
    {
        /// <summary>
        /// Получить информацию с контактами Адрес, телефоны и т.п.
        /// </summary>
        /// <returns></returns>
        Task<ContactsDto> GetContacts();
    }
}
