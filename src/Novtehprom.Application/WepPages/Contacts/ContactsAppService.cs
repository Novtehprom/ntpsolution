﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.ObjectMapping;
using Novtehprom.Authorization;
using Novtehprom.EntityFrameworkCore.Repositories;
using Novtehprom.WebPages.Contacts;
using Novtehprom.WepPages.Contacts.Dto;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.Contacts
{
    public class ContactsAppService : ApplicationService, IContactsAppService
    {
        public ContactsAppService(IRepository<Contact, int> repository, ILocalizationManager locManager, IApplicationLanguageTextManager locTextManager)
        {
            this.repository = repository;
            this.locManager = locManager;
            this.locTextManager = locTextManager;
        }

        private readonly IRepository<Contact, int> repository;
        private readonly ILocalizationManager locManager;
        private readonly IApplicationLanguageTextManager locTextManager;
        public async Task<ContactsDto> GetContacts()
        {
            //var ss = locManager.GetAllSources();
            //var ntp = locManager.GetSource("Novtehprom");
            //var ww = ntp.GetAllStrings();
            //var cc = locManager.GetString("Novtehprom", "ProgramCostContentTitle");
            //var dd = locManager.GetString("Novtehprom", "ContName1");


            //var contName = locTextManager.GetStringOrNull(null, "Novtehprom", cult, "ProgramCostContentTitle");
            //var contName1 = locTextManager.GetStringOrNull(null, "Novtehprom", cult, "ContName1");
            //await locTextManager.UpdateStringAsync(null, "Novtehprom", cult, "ContName1", "Всякая ерунда плюс 11111");
            //await locTextManager.UpdateStringAsync(null, "Contact", cult, "ContName", "Всякая ерунда плюс плюс 222");
            //contName1 = locTextManager.GetStringOrNull(null, "Novtehprom", cult, "ContName1");
            //var contName1 = locTextManager.GetStringOrNull(null, "Contact", cult, "ContName");
            //var cont = await repository.GetAllListAsync();
            var cont = repository.GetAllIncluding(c => c.Communications).FirstOrDefault(c => !c.IsDeleted);
            ContactsDto ret = null;
            await Task.Factory.StartNew(()=> 
            {
                if (cont != null)
                {
                    ret = ObjectMapper.Map<ContactsDto>(cont);
                    ret.AddressFact = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.AddressFact);
                    ret.AddressReg = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.AddressReg);
                    ret.NameFull = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.NameFull);
                    ret.NameShort = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.NameShort);
                    ret.BankName = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.BankName);
                    ret.BossFio.Name = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.BossFio.Name);
                    ret.BossFio.Surname = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.BossFio.Surname);
                    ret.BossFio.Patronymic = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.BossFio.Patronymic);
                    ret.AboutCompanyText = locManager.GetString(NovtehpromConsts.LocalizationSourceName, ret.AboutCompanyText);
                    foreach (var comm in ret.Communications)
                    {
                        comm.Name = locManager.GetString(NovtehpromConsts.LocalizationSourceName, comm.Name);
                    }
                }
            });
            return ret;
        }
    }
}
