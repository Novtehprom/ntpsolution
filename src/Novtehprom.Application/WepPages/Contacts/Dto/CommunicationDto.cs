﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WepPages.Contacts.Dto
{
    /// <summary>
    /// Способ связи: телефон или эл почта или т.п
    /// </summary>
    public class CommunicationDto : EntityDto<int>
    {
        /// <summary>
        /// Порядковый номер в списке
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// Наименование: телефон, эл-почта, skype и т.п.
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        [MaxLength(256)]
        public string Value { get; set; }
    }
}
