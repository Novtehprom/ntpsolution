﻿using Abp.Application.Services.Dto;
using Abp.EntityHistory;
using Novtehprom.WebPages.BaseTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novtehprom.WepPages.Contacts.Dto
{
    /// <summary>
    /// Транспорт информации о компании. Адрес, р.с телефоны т т.п.
    /// </summary>
    //[UseCase(Description ="Работа с таблицей контактов")]
    public class ContactsDto: EntityDto<int>
    {
        /// <summary>
        /// Полное наименование
        /// </summary>
        [Required]
        public string NameFull { get; set; }
        /// <summary>
        /// Юоидический адрес
        /// </summary>
        public string AddressReg { get; set; }
        /// <summary>
        /// ИНН
        /// </summary>
        [StringLength(10)]
        [Required]
        public string INN { get; set; }
        /// <summary>
        /// КПП
        /// </summary>
        [StringLength(9)]
        [Required]
        public string KPP { get; set; }
        /// <summary>
        /// Наименование банка
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// Расчетный счет
        /// </summary>
        [StringLength(20)]
        public string Account { get; set; }
        /// <summary>
        /// Кор счет банка
        /// </summary>
        [StringLength(20)]
        public string CorrAccount { get; set; }
        /// <summary>
        /// Бик банка
        /// </summary>
        [StringLength(9)]
        public string BainkBIK { get; set; }
        /// <summary>
        /// Фактический адрес
        /// </summary>
        public string AddressFact { get; set; }
        /// <summary>
        /// ОГРН
        /// </summary>
        [StringLength(13)]
        [Required]
        public string OGRN { get; set; }
        /// <summary>
        /// ОКПО
        /// </summary>
        [StringLength(8)]
        [Required]
        public string OKPO { get; set; }
        /// <summary>
        /// ОКВЕД
        /// </summary>
        [StringLength(50)]
        [Required]
        public string OKVED { get; set; }
        /// <summary>
        /// Краткое наименование
        /// </summary>
        [Required]
        public string NameShort { get; set; }
        /// <summary>
        /// О компании
        /// </summary>
        public string AboutCompanyText { get; set; }
        /// <summary>
        /// Фио директора
        /// </summary>
        public Fio BossFio { get; set; }
        /// <summary>
        /// Способы связи, телефоны, эл почта и т.п
        /// </summary>
        public List<CommunicationDto> Communications { get; set; }
    }
}
