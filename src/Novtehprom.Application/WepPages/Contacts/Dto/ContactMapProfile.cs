﻿using Abp.Localization;
using AutoMapper;
using Novtehprom.WebPages.BaseTypes;
using Novtehprom.WebPages.Contacts;
using System.Linq;

namespace Novtehprom.WepPages.Contacts.Dto
{
    public class ContactMapProfile :Profile
    {
        public ContactMapProfile()
        {
            CreateMap<ContactsDto, Contact>()
                .ForMember(c => c.Communications, opt =>
                    opt.MapFrom(m => m.Communications
                        .Select(c => new Communication
                        {
                            Id = c.Id,
                            Name = c.Name,
                            OrderNum = c.OrderNum,
                            Value = c.Value
                        }
                    )))
                .ForMember(c => c.BossFio, opt => opt.MapFrom(d => d.BossFio));
            CreateMap<Contact, ContactsDto>()
                .ForMember(c => c.Communications, opt => 
                    opt.MapFrom(m => m.Communications
                        .Select(c => new CommunicationDto
                        {
                            Id = c.Id,
                            Name = c.Name,
                            OrderNum = c.OrderNum,
                            Value = c.Value
                        }
                    )))
                .ForMember(c => c.BossFio, opt =>
                    opt.MapFrom(c => new Fio
                    {
                        Name = c.BossFio.Name,
                        Surname = c.BossFio.Surname,
                        Patronymic = c.BossFio.Patronymic
                    }));
        }
    }
}
