﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Novtehprom.WepPages.Contacts.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.Contacts
{
    /// <summary>
    /// Интерфейс службы, возвращаюшей контактную информацию
    /// </summary>
    public interface IContactsAppService: IApplicationService
    {
        /// <summary>
        /// Получить информацию с контактами Адрес, телефоны и т.п.
        /// </summary>
        /// <returns></returns>
        Task<ContactsDto> GetContacts();
    }
}
