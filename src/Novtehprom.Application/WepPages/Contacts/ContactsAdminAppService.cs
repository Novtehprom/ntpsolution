﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Novtehprom.Authorization;
using Novtehprom.EntityFrameworkCore.Repositories;
using Novtehprom.WebPages.Contacts;
using Novtehprom.WepPages.Contacts.Dto;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Novtehprom.WepPages.Contacts
{
    [AbpAuthorize(PermissionNames.Pages_Contacts)]
    public class ContactsAdminAppService : AsyncCrudAppService<Contact, ContactsDto, int, ContactsDto, ContactsDto, ContactsDto>, IContactsAdminAppService
    {
        public ContactsAdminAppService(IRepository<Contact, int> repository) : base(repository)
        {
        }
        public override async Task<ContactsDto> CreateAsync(ContactsDto input)
        {
            CheckCreatePermission();
            var contact = MapToEntity(input);
            await this.Repository.InsertAndGetIdAsync(contact);
            return MapToEntityDto(contact);
        }
        public async Task<ContactsDto> GetContacts()
        {
            CheckDeletePermission();
            var rep = this.Repository;
            var cont = await rep.GetAllListAsync();
            if (cont != null && cont.Count > 0)
            {
                //return ObjectMapper.Map<ContactsDto>(cont.FirstOrDefault());
                var ent = cont.FirstOrDefault();
                try
                {
                    var ret = MapToEntityDto(ent);
                    return ret;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else return null;
        }
        public override async Task DeleteAsync(EntityDto<int> input)
        {
            CheckDeletePermission();
            Repository.Delete(input.Id);
            //var contact = await Repository.FirstOrDefaultAsync(c => c.Id == input.Id);
            //if(contact!= null) Repository.Delete(contact);
            //await Repository.DeleteAsync(input.Id);
        }
    }
}
