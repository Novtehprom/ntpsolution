﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Novtehprom.Authorization;

namespace Novtehprom
{
    [DependsOn(
        typeof(NovtehpromCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class NovtehpromApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<NovtehpromAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(NovtehpromApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
