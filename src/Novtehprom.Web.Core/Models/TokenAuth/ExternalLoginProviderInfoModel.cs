﻿using Abp.AutoMapper;
using Novtehprom.Authentication.External;

namespace Novtehprom.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
