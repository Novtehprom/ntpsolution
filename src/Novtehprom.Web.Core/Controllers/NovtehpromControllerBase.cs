using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Novtehprom.Controllers
{
    public abstract class NovtehpromControllerBase: AbpController
    {
        protected NovtehpromControllerBase()
        {
            LocalizationSourceName = NovtehpromConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
